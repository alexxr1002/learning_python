import pprint
import random

map = ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_']

def display_map(list_object):
    print(map[0:5])
    print(map[5:10])
    print(map[10:15])
    print(map[15:20])
    print(map[20:25])

def todoroki(list_object):
    t_location = random.randint(0,24)
    list_object[t_location] = 'T'
    return list_object
    return t_location

def midoriya(list_object):
    while True:
        m_location = random.randint(0,24) #jumping to a new spot
        if list_object[m_location] == 'T':                   #is todoroki here? Yes?
            continue
        else:                                   #no?
            map[m_location] = 'M'
            return list_object
            return m_location
            break

def bakugo(list_object):
    bakugo_explosions = 36
    midoriya_hp = 3
    while bakugo_explosions > 0:
        print('Terrain: ')
        display_map(list_object)
        blast_location = random.randint(0,24) # bakugo's attack m_location
        if list_object[blast_location] == '_':
            print('!!!BOOOM!!!')
            print('Bakugo: \'tch... nothing..\'')
            bakugo_explosions -= 1
            print('Remaining Explosions: ' + str(bakugo_explosions))
            print('Deku\'s HP: ' + str(midoriya_hp))
        elif list_object[blast_location] == 'T':
            print('!!!BOOOM!!!')
            print('Bakugo: \'Todoroki?!?!\'')
            print('Bakugo: *cries in frozen*...')
            bakugo_explosions -= 3
            print('Remaining Explosions: ' + str(bakugo_explosions))
            print('Deku\'s HP: ' + str(midoriya_hp))
        elif list_object[blast_location] == 'M':
            print('Bakugo: \'THIS IS THE SPOT\'')
            print('!!!BOOOM!!!')
            print('Midoriya: \' OH FUCK MY HP \'')
            midoriya_hp -= 1
            bakugo_explosions -= 1
            if midoriya_hp == 0:
                print('Deku\'s HP: ' + str(midoriya_hp))
                print('BAKUGO WINS!! PLUS ULTRA!!')
                quit()
            print('Remaining Explosions: ' + str(bakugo_explosions))
            print('Deku\'s HP: ' + str(midoriya_hp))
            list_object[blast_location] = '_'
            midoriya(list_object)

    print('DEKU WINS!! PLUS ULTRA!!')

map = todoroki(map)

map = midoriya(map)

bakugo(map)
