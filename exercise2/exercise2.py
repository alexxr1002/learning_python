import random


def kramer_time():
    """return kramers invention if it is 5 words
    """
    with open('kramers_inventions.txt', 'r') as f:
        ideas = [line.strip() for line in f.readlines()]
        attempt = 0

        for attempt in range(0, 2):
            num_words = random.randrange(3, 7)
            invention = random.sample(ideas, num_words)
            print('Kramer: ".... what if I told you I\'m gonna make a ' + str(invention) + '.\"')

            if num_words == 5:
                print('Jerry: "Ya know, that\'s not a bad idea... Come on in."')
                print('*Kramer, Elaine, and George barge into the apartment*')
                return invention
            else:
                print('Jerry: "C\'mon man, I\'m serious here!!"')
                return []


def elaine_time():
    rent_guess = input('Elaine: "Hey Jer, I think Im going to sign this lease for the building down the street. What do you think the rent should be?"')
    if int(rent_guess) <= 600:
        print('Elaine: I\'ll take it!')
        return 'decided to lease a new apartment.'
    elif 600 < int(rent_guess) < 800:
        print('Elaine: Ehhhh.. maybe..')
        return 'came over and couldn\'t decide on a new apartment.'
    elif int(rent_guess) >= 800:
        print('Elaine: There\'s no way I can afford that!!')
        return 'turned down an apartment offer'


def george_time(invention):
    chance = random.randrange(0, 1)
    print('George: "Hey Jerry, mind if I use the phone?"')
    if chance == 1:
        print('George: "My friend Kramer has an idea I think you might like for an invention, ....its called the ' + invention + '.')
        return'actually did'
    else:
        print('George: Gaahh I can\'t do it...')
        return 'sadly did not'


def join_str(list_object):
    return '{} {} {} {} {}'.format(list_object[0], list_object[1], list_object[2], list_object[3], list_object[4])


print('*KNOCK *KNOCK *KNOCK *KNOCK *KNOCK *KNOCK')
print('Kramer: "JERRY. I\'ve got something for ya!"')
print('Jerry: "Not now Kramer, I\'m busy."')

invention = kramer_time()
if len(invention) != 5:
    print('**The 4 of them went to the diner that night and sat in silence**')
else:
    elaines_choice = elaine_time()
    georges_choice = george_time(invention)
    print('**Cut to Jerry\'s routine**')
    print('Jerry: So my neighbor came over and pitched this dumb idea he calls the ' + join_str(invention) + '. Whats the deal with that? Then my friend Elaine ' + str(elaines_choice) + '.' 'And George ' + str(georges_choice) + ' call the lady back.')
