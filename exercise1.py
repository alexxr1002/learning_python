
vocab = ['dog', 'cat', 'trex']

print('Give me some words! (Type "quit" to end)')
while True:
    word = input()
    if word == 'quit':
        break
    vocab = vocab + [word]

print('Here are all of my words: ')
for word in vocab:
    print(word)

print('reversed upper of each string')
rev_Upper = [x.upper()[::-1] for x in vocab]
# for x in rev_Upper:
#     print(str(x)[::-1])
print(rev_Upper)

print('num chars each string:')
num_Chars = []
for word in vocab:
    num_Chars.append(len(word))
print(num_Chars)

print('num words each user input:')
num_Words = []
for word in vocab:
    num_Words.append(len(word.split()))
print(num_Words)

print('Num words in all user inputs:')
print('Total words typed:')
sum_Words = 0
for word in vocab:
    sum_Words = sum_Words + int(len(word.split()))
print(str(sum_Words))
